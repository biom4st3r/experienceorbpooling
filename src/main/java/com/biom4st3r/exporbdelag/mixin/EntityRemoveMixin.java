package com.biom4st3r.exporbdelag.mixin;

import com.biom4st3r.exporbdelag.ExperienceOrbTracker;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.entity.Entity;
import net.minecraft.entity.ExperienceOrbEntity;

@Mixin(Entity.class)
public abstract class EntityRemoveMixin
{
    @Inject(at=@At("HEAD"),method = "remove",cancellable = true)
    public void remove(CallbackInfo ci)
    {
        if(((Entity)(Object)this) instanceof ExperienceOrbEntity)
        {
            ExperienceOrbTracker.deRegister((ExperienceOrbEntity)(Object)this);
        }
    }



}