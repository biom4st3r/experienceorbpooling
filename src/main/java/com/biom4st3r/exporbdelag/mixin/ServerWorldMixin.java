package com.biom4st3r.exporbdelag.mixin;

import com.biom4st3r.exporbdelag.ExperienceOrbTracker;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.server.world.ServerWorld;

@Mixin(ServerWorld.class)
public abstract class ServerWorldMixin
{
    @Inject(at = @At(value = "HEAD"),method = "addEntity",cancellable = true)
    private void addEntity(Entity e, CallbackInfoReturnable<Boolean> ci)
    {
        if(e instanceof ExperienceOrbEntity)
        {
            if(!ExperienceOrbTracker.Register((ExperienceOrbEntity)e))
            {
                ci.setReturnValue(false);
                ci.cancel();
            }
        }
    }
}