package com.biom4st3r.exporbdelag.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import com.biom4st3r.exporbdelag.EditableExperience;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.world.World;

@Mixin(ExperienceOrbEntity.class)
public abstract class ExperienceOrbMixin extends Entity implements EditableExperience {

    public ExperienceOrbMixin(EntityType<?> type, World world) {
		super(type, world);
    }

    @Shadow
    private int amount;

    @Override
    public void addExperience(int val) {
        this.amount += val;
    }
}