package com.biom4st3r.exporbdelag.mixin;

import java.util.function.Consumer;
import com.biom4st3r.exporbdelag.ExperienceOrbTracker;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import net.minecraft.entity.Entity;

import net.minecraft.world.World;

@Mixin(World.class)
public abstract class WorldMixin
{

    @Inject(at=@At("RETURN"),method="tickEntity",cancellable=false)
    public void tickEntity(Consumer<Entity> consumer_1, Entity entity_1, CallbackInfo ci)
    {
        ExperienceOrbTracker.onTick();
        
    } 

}
