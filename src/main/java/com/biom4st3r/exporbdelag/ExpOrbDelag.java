package com.biom4st3r.exporbdelag;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityCategory;
import net.minecraft.entity.EntityType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ExpOrbDelag implements ModInitializer {
	public static final String MODID = "biom4st3rexporbdelag";

	public static EntityType<ExperienceOrbBundleEntity> orbBundle;


	@Override
	public void onInitialize() 
	{
		orbBundle = (EntityType)Registry.register(
			Registry.ENTITY_TYPE, 
			new Identifier(MODID, "experienceorbbundle"),
			FabricEntityTypeBuilder.create(
				EntityCategory.MISC, 
				(t,w)->
				{
					return new ExperienceOrbBundleEntity(t,w);
				}).size(0.25f, 0.25f).build()
			);
	}
}
