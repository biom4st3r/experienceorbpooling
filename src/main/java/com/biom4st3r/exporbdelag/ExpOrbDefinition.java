package com.biom4st3r.exporbdelag;

import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BoundingBox;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkStatus;

public class ExpOrbDefinition {

    Vec3d pos;
    int value;
    World world;

    public ExpOrbDefinition(ExperienceOrbEntity e)
    {
        this.pos = e.getPos();
        value = e.getExperienceAmount();
        world = e.getEntityWorld();
    }

    public ExperienceOrbEntity spawn()
    {
        ExperienceOrbEntity newOrb = new ExperienceOrbEntity(this.world, this.pos.x, this.pos.y, this.pos.z, value);
        if(newOrb.world.random.nextInt() % 10 == 0) // adding experience to the player is not linear
        {
                newOrb.world.getEntities(ExperienceOrbEntity.class, new BoundingBox(newOrb.getBlockPos()).expand(4D)).stream().forEach((orbFromList) -> {
                    ExperienceOrbBundleEntity orbBundle = new ExperienceOrbBundleEntity(newOrb.world, newOrb.x, newOrb.y, newOrb.z, newOrb.getExperienceAmount());
                    if(!orbFromList.removed && (orbFromList.getExperienceAmount() == newOrb.getExperienceAmount()))
                    {
                        //EditableExperience editableExpOrb = (EditableExperience)e;
                        //editableExpOrb.addExperience(orbFromList.getExperienceAmount());
                        orbBundle.addExperience(orbFromList.getExperienceAmount());
                        orbFromList.remove();
                        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~left off here

                    }
                });
        }
        Chunk chunk = ((ServerWorld)world).getChunk(MathHelper.floor(newOrb.x / 16.0D), MathHelper.floor(newOrb.z / 16.0D), ChunkStatus.FULL, newOrb.teleporting);
        chunk.addEntity(newOrb);
        ((ServerWorld)world).loadEntity(newOrb);
        return newOrb;
    }
    

    public static ExpOrbDefinition create(ExperienceOrbEntity e)
    {
        return new ExpOrbDefinition(e);
    }
}