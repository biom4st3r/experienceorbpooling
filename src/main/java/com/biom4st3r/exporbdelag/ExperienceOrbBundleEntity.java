package com.biom4st3r.exporbdelag;

import java.util.Map.Entry;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class ExperienceOrbBundleEntity extends ExperienceOrbEntity
{
    public int actualExperience = 0;
    public int initialSize = 0;


    public ExperienceOrbBundleEntity(World world_1, double xCoord, double yCoord, double zCoord, int experience) 
    {
        super(world_1, xCoord, yCoord, zCoord, experience);
        initialSize = experience;
    }
  
    public ExperienceOrbBundleEntity(EntityType<Entity> t, World world_1) 
    {
        super(ExpOrbDelag.orbBundle, world_1);
        
    }

    public void addExperience(int val)
    {
        actualExperience+=val;
    }

	private int halved(int int_1) {
        return int_1 / 2;
    }
  
    private int doubled(int int_1) {
        return int_1 * 2;
    }
    
    public void onPlayerCollision(PlayerEntity pe) {
        if (!this.world.isClient) 
        {
            if (this.pickupDelay == 0 && pe.experienceOrbPickupDelay == 0) 
            {
                pe.experienceOrbPickupDelay = 2;
                pe.sendPickup(this, 1);
                for(int cycleallotment = initialSize; actualExperience > 0; actualExperience-=initialSize)
                {
                    Entry<EquipmentSlot, ItemStack> map$Entry_1 = EnchantmentHelper.getRandomEnchantedEquipment(Enchantments.MENDING, pe);
                    if (map$Entry_1 != null) 
                    {
                        ItemStack itemStack_1 = (ItemStack)map$Entry_1.getValue();
                        if (!itemStack_1.isEmpty() && itemStack_1.isDamaged()) 
                        {
                            int addedDurability = Math.min(this.doubled(cycleallotment), itemStack_1.getDamage());
                            cycleallotment -= this.halved(addedDurability);
                            itemStack_1.setDamage(itemStack_1.getDamage() - addedDurability);
                        }
                    }

                    if (cycleallotment > 0) {
                        pe.addExperience(cycleallotment);
                    }

                    this.remove();
                }
            }

        }
    }


}