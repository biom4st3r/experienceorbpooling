package com.biom4st3r.exporbdelag;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

//import com.biom4st3r.exporbdelag.mixin.ExperienceSet;

import net.minecraft.entity.ExperienceOrbEntity;

public class ExperienceOrbTracker 
{
    private static int maxActive = 200;
    static boolean locked = false;
    private static Queue<ExpOrbDefinition> waitingList = new LinkedList<ExpOrbDefinition>();
    private static Queue<ExperienceOrbEntity> activeList = new LinkedList<ExperienceOrbEntity>();

    public static boolean Register(ExperienceOrbEntity e)
    {
        System.out.println(String.format("(Registered) active: %o | waiting: %o", activeList.size(),waitingList.size()));
        waitingList.add(ExpOrbDefinition.create(e));
        tryActivate();
        return false;
    }

    public static boolean deRegister(ExperienceOrbEntity e)
    {
        System.out.println(String.format("(deRegistered) active: %o | waiting: %o", activeList.size(),waitingList.size()));
        return activeList.remove(e);
    }

    public static void onTick()
    {
        tryActivate();
    }

    private static void tryActivate()
    {
        if(activeList.size() < maxActive && waitingList.size() > 0)
        {
            try
            {
                ExpOrbDefinition eDef =  waitingList.remove();
                ExperienceOrbEntity newOrb = eDef.spawn();      
                activeList.add(newOrb);
            }
            catch(NoSuchElementException e)
            {
                
            }
        }
    }
}